/**
 * Copyright (c) 2018 Thomas Rokicki
 */

package application.models.response.user;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "users", schema = "v1")
public class User {

	public static final String ID = "id";
	public static final String USER_ID = "user_id";
	public static final String USERNAME = "username";
	public static final String PASSWORD = "password";
	public static final String SALT = "salt";
	public static final String HASH_ALGORITHM = "hash_algorithm";
	public static final String GIVERN_NAME = "given_name";
	public static final String SURNAME = "surname";

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	@JsonIgnore
	private int id;

	@Column(name = "user_id")
	private String user_id;

	@Column(name = "username")
	private String username;

	@JsonIgnore
	@Column(name = "password")
	private String password;

	@JsonIgnore
	@Column(name = "salt")
	private String salt;

	@JsonIgnore
	@Column(name = "hash_algorithm")
	private String hash_algorithm;

	@Column(name = "given_name")
	private String given_name;

	@Column(name = "surname")
	private String surname;

	public User() {}

	public User(int id, String user_id, String username, String password, String salt, String hash_algorithm,
			String given_name, String surname) {
		super();
		this.id = id;
		this.user_id = user_id;
		this.username = username;
		this.password = password;
		this.salt = salt;
		this.hash_algorithm = hash_algorithm;
		this.given_name = given_name;
		this.surname = surname;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	@JsonIgnore
	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getGiven_name() {
		return given_name;
	}

	public void setGiven_name(String given_name) {
		this.given_name = given_name;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public String getUser_id() {
		return user_id;
	}

	public void setUser_id(String user_id) {
		this.user_id = user_id;
	}

	public String getSalt() {
		return salt;
	}

	public void setSalt(String salt) {
		this.salt = salt;
	}

	public String getHash_algorithm() {
		return hash_algorithm;
	}

	public void setHash_algorithm(String hash_algorithm) {
		this.hash_algorithm = hash_algorithm;
	}

}
