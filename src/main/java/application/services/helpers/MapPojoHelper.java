/**
 * Copyright (c) 2018 Thomas Rokicki
 */

package application.services.helpers;

import java.io.IOException;

import org.json.JSONObject;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class MapPojoHelper {

	public static <T extends Object> JSONObject getAsJSONObject(T object) throws JsonProcessingException {
		ObjectMapper mapper = new ObjectMapper();
		String jsonString = mapper.writeValueAsString(object);
		return new JSONObject(jsonString);
	}

	public static <T extends Object> String getAsJSONString(T object) throws JsonProcessingException {
		ObjectMapper mapper = new ObjectMapper();
		return mapper.writeValueAsString(object);
	}

	public static <T extends Object> T parseJson(String json, Class<T> type) throws IOException {
		ObjectMapper mapper = new ObjectMapper();
		return mapper.readValue(json, type);
	}

	public static <T extends Object> T parseJSONObject(JSONObject jsonObject, Class<T> type) throws IOException {
		ObjectMapper mapper = new ObjectMapper();
		return mapper.readValue(jsonObject.toString(), type);
	}
}
